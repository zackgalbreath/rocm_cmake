
Instructions needed to build rocm 3.9


```
git clone https://github.com/ROCm-Developer-Tools/ROCclr.git -b rocm-3.9.x
git clone https://github.com/RadeonOpenCompute/ROCm-OpenCL-Runtime.git -b rocm-3.9.x
export ROCclr_DIR="$(readlink -f ROCclr)"
export OPENCL_DIR="$(readlink -f ROCm-OpenCL-Runtime)"
cd "$ROCclr_DIR"
mkdir -p build; cd build
cmake -DOPENCL_DIR="$OPENCL_DIR" -DCMAKE_INSTALL_PREFIX=/opt/rocm/rocclr ..
make -j$(nproc)

cmake -S ../hip/src/HIP/ -B hip_build/ -DCMAKE_BUILD_TYPE=Release -DHIP_COMPILER=clang  -DHIP_PLATFORM=rocclr -DCMAKE_PREFIX_PATH=/workspaces/rocm/rocm_device_install/
```

Copy the configured cmake files over to `HipLangConfig`


