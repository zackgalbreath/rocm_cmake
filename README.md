# HIP-clang support in CMake ##

This repository is a collection of infrastructure required to develop and test
HIP-clang support in CMake proper

## Docker ##

Collection of images that contain the entire stack required for ROCm compiler + CMake.

## Resources ##

- https://github.com/ROCm-Developer-Tools/HIP
- https://github.com/ROCm-Developer-Tools/HIP/blob/master/hip-config.cmake.in
- https://github.com/RadeonOpenCompute/rocm-cmake
- https://github.com/RadeonOpenCompute/ROCm#ROCm-Components
- https://github.com/RadeonOpenCompute/ROCm-docker
