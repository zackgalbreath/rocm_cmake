#!/bin/sh

set -e
set -x

cd "$(dirname "$0")"

mkdir -p hip-lang
cp ../src/HipLangConfig/hip* hip-lang/

docker build -t zackgalbreath/hip:ubuntu-rocm4.0_repro -f ubuntu/rocm4.0_repro/Dockerfile .
