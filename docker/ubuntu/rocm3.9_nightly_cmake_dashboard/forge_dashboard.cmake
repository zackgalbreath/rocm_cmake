set(CTEST_SITE "forge.kitware")
set(CTEST_BUILD_NAME "Ubuntu-Ninja-Multi")
set(CTEST_BUILD_CONFIGURATION Debug)
set(CTEST_CMAKE_GENERATOR "Ninja Multi-Config")

set(build_dir "cmake-ninja-multi")
set(source_dir "${build_dir}-src")
set(CTEST_DASHBOARD_ROOT "/scratch/My Tests/")
set(CTEST_SOURCE_DIRECTORY "${CTEST_DASHBOARD_ROOT}/${source_dir}")
set(CTEST_BINARY_DIRECTORY "${CTEST_DASHBOARD_ROOT}/${build_dir}")

# Write initial cache.
set(dashboard_cache "
CMake_TEST_HIP:STRING=ON
")

include(${CTEST_SCRIPT_DIRECTORY}/cmake_common.cmake)
