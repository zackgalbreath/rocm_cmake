#!/bin/bash
docker run -it --device=/dev/kfd \
  --device=/dev/dri \
  --security-opt seccomp=unconfined \
  --group-add video \
  robertmaynard/hip:ubuntu-rocm3.9-nightly-dashboard
