#!/bin/bash
docker run --device=/dev/kfd \
  --device=/dev/dri \
  --security-opt seccomp=unconfined \
  --group-add video \
  kitware/cmake:ci-hip-4.2-ubuntu-18.04
