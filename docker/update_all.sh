#!/bin/sh

set -e
set -x


cd "$(dirname "$0")"

mkdir -p hip-lang
cp ../src/HipLangConfig/hip* hip-lang/

docker build -t robertmaynard/hip:ubuntu-rocm3.7-base -f ubuntu/rocm3.7_base/Dockerfile .
docker build -t robertmaynard/hip:ubuntu-rocm3.7-cmake -f ubuntu/rocm3.7_cmake/Dockerfile .
docker build -t robertmaynard/hip:ubuntu-rocm3.7 -f ubuntu/rocm3.7/Dockerfile .

docker build -t robertmaynard/hip:ubuntu-rocm3.8-base -f ubuntu/rocm3.8_base/Dockerfile .
docker build -t robertmaynard/hip:ubuntu-rocm3.8-cmake -f ubuntu/rocm3.8_cmake/Dockerfile .
docker build -t robertmaynard/hip:ubuntu-rocm3.8 -f ubuntu/rocm3.8/Dockerfile .

docker build -t robertmaynard/hip:ubuntu-rocm3.9-base -f ubuntu/rocm3.9_base/Dockerfile .
docker build -t robertmaynard/hip:ubuntu-rocm3.9-cmake -f ubuntu/rocm3.9_cmake/Dockerfile .
docker build -t robertmaynard/hip:ubuntu-rocm3.9 -f ubuntu/rocm3.9/Dockerfile .

# sudo docker login --username=<docker_hub_name>
# docker push robertmaynard/hip
# sudo docker system prune
