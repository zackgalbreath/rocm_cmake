# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

#[=======================================================================[.rst:
FindROCMToolkit
---------------

This find module locates all the AMD ROCm toolkit libraries and provides a
consistent set of targets.

Search Behavior
^^^^^^^^^^^^^^^

Since the ROCm libraries can be installed as separate components we use
the following search pattern:

1. If the ``ROCMToolkit_ROOT`` cmake configuration variable (e.g.,
   ``-DROCMToolkit_ROOT=/some/path``) *or* environment variable is defined, it
   will be searched.  If both an environment variable **and** a
   configuration variable are specified, the *configuration* variable takes
   precedence.

2. If the ROCM_PATH environment variable is defined, it will be searched.

3. If the HIP_PATH environment variable is defined, it will be searched.

4. The user's path is searched for ``hipcc`` using :command:`find_program`.  If
   this is found, no subsequent search attempts are performed.  Users are
   responsible for ensuring that the first ``hipcc`` to show up in the path is
   the desired path in the event that multiple ROCm versions are installed.


5. On Unix systems, if the symbolic link ``/opt/rocm`` exists, this is
   used.

6. The platform specific default install locations are searched.

   +-------------+-------------------------------------------------------------+
   | Platform    | Search Pattern                                              |
   +=============+=============================================================+
   | Unix        | ``/opt/rocm/``                                              |
   |             | ``/opt/rocm-X.Y.Z``                                         |
   +-------------+-------------------------------------------------------------+


   Where ``X.Y.Z`` would be a specific version of the ROCm Toolkit, such as
   ``/opt/rocm-3.3.0``

   .. note::

       When multiple versions of ROCm are installed in the default location of a
       system (e.g., both ``/opt/rocm-3.3.0`` and ``/opt/rocm-3.4.0``
       exist but the ``/opt/rocm.`` symbolic link does **not** exist), this
       package is marked as **not** found.

       There are too many factors involved in making an automatic decision in
       the presence of multiple ROCm versions being installed.  In this
       situation, users are encouraged to either (1) set ``ROCMToolkit_ROOT`` or
       (2) ensure that the correct ``hipcc`` executable shows up in ``$PATH`` for
       :command:`find_program` to find.

Options
^^^^^^^

``VERSION``
    If specified, describes the version of the CUDA Toolkit to search for.

``REQUIRED``
    If specified, configuration will error if a suitable CUDA Toolkit is not
    found.

``QUIET``
    If specified, the search for a suitable CUDA Toolkit will not produce any
    messages.

``EXACT``
    If specified, the CUDA Toolkit is considered found only if the exact
    ``VERSION`` specified is recovered.

Imported targets
^^^^^^^^^^^^^^^^

An :ref:`imported target <Imported targets>` named ``ROCm::toolkit`` is provided
which specifies include and link directories.

This module defines :prop_tgt:`IMPORTED` targets for each
of the following libraries that are part of the ROCm toolkit:

- :ref:`HIP Runtime Library<hip_rt_lib>`
- :ref:`HIP Driver Library<hip_driver_lib>`
- :ref:`rocBLAS<rocm_rocBLAS>`
- :ref:`hipBLAS<rocm_hipBLAS>`
- :ref:`rocRAND<rocm_rocRAND>`
- :ref:`rocFFT<rocm_rocFFT>`
- :ref:`rocPRIM<rocm_rocPRIM>`
- :ref:`rocSPARSE<rocm_rocSPARSE>`
- :ref:`hipSPARSE<rocm_hipSPARSE>`
- :ref:`rocSOLVER<rocm_rocSOLVER>`
- :ref:`rocALUTION<rocm_rocALUTION>`
- :ref:`rocTHRUST<rocm_rocTHUST>`
- :ref:`hipCUB<rocm_hipCUB>`


.. _`hip_rt_lib`:

HIP host Library
""""""""""""""""""""

The HIP host library are what applications will typically
need to link against when they want to make calls such as `hipMalloc`, and `hipFree`,
but don't need to call device kernels.

Targets Created:

- ``ROCm::hip_host``

.. _`hip_driver_lib`:

HIP device Library
""""""""""""""""""""

The HIP device library are what applications will typically need
to use when they want to launch device kernels.

Targets Created:

- ``ROCm::hip_device``

.. _`rocm_rocBLAS`:

rocBLAS
""""""

The `rocBLAS <https://rocmdocs.amd.com/en/latest/ROCm_Libraries/ROCm_Libraries.html#rocblas>`_ library.
A BLAS implementation on top of AMD’s Radeon Open Compute ROCm runtime and toolchains.

Targets Created:

- ``ROCm::rocblas``

.. _`rocm_hipBLAS`:

hipBLAS
""""""

The `hipBLAS <https://rocmdocs.amd.com/en/latest/ROCm_Libraries/ROCm_Libraries.html#hipblas>`_ library.
hipBLAS is a BLAS marshalling library, with multiple supported backends. It sits between the application and a ‘worker’ BLAS library.

Targets Created:

- ``ROCm::hipblas``

.. _`rocm_rocRAND`:

rocRAND
""""""

The `rocRAND <https://rocmdocs.amd.com/en/latest/ROCm_Libraries/ROCm_Libraries.html#rocrand>`_ library.
The rocRAND project provides functions that generate pseudo-random and quasi-random numbers.

Targets Created:

- ``ROCm::rocrand``

.. _`rocm_rocFFT`:

rocFFT
""""""

The `rocFFT <https://rocmdocs.amd.com/en/latest/ROCm_Libraries/ROCm_Libraries.html#rocfft>`_ library.
rocFFT is a software library for computing Fast Fourier Transforms (FFT) written in HIP.

Targets Created:

- ``ROCm::rocfft``

.. _`rocm_rocPRIM`:

rocPRIM
""""""

The `rocPRIM <https://github.com/ROCmSoftwarePlatform/rocPRIM>`_ library.
The rocPRIM is a header-only library providing HIP parallel primitives for developing performant GPU-accelerated code on AMD ROCm platform.

Targets Created:

- ``ROCm::rocprim``

.. _`rocm_rocSPARSE`:

rocSPARSE
""""""

The `rocSPARSE <https://rocmdocs.amd.com/en/latest/ROCm_Libraries/ROCm_Libraries.html#rocsparse>`_ library.
rocSPARSE is a library that contains basic linear algebra subroutines for sparse matrices and vectors written in HiP for GPU devices. It is designed to be used from C and C++ code.

Targets Created:

- ``ROCm::rocsparse``

.. _`rocm_hipSPARSE`:

hipSPARSE
""""""

The `hipSPARSE <https://rocmdocs.amd.com/en/latest/ROCm_Libraries/ROCm_Libraries.html#hipsparse>`_ library.
hipSPARSE is a SPARSE marshalling library, with multiple supported backends. It sits between the application and a ‘worker’ SPARSE library.

Targets Created:

- ``ROCm::hipsparse``

.. _`rocm_rocSOLVER`:

rocSOLVER
""""""

The `rocSOLVER <https://rocmdocs.amd.com/en/latest/ROCm_Libraries/ROCm_Libraries.html#rocsolver>`_ library.
An implementation of Lapack routines on top of AMD’s Radeon Open Compute Platform (ROCm) runtime and toolchains.

Targets Created:

- ``ROCm::rocsolver``

.. _`rocm_rocALUTION`:

rocALUTION
""""""

The `rocALUTION <https://rocmdocs.amd.com/en/latest/ROCm_Libraries/ROCm_Libraries.html#rocalution>`_ library.
rocALUTION is a sparse linear algebra library with focus on exploring fine-grained parallelism, targeting modern processors and accelerators including multi/many-core CPU and GPU platforms.

Targets Created:

- ``ROCm::rocalution``

.. _`rocm_rocTHUST`:

rocTHRUST
""""""

The `rocTHRUST <https://rocmdocs.amd.com/en/latest/ROCm_Libraries/ROCm_Libraries.html#rocthrust>`_ library.
rocTHRUST is the THRUST library ported to HIP/ROCm platform, which uses the rocPRIM library.

Targets Created:

- ``ROCm::rocthrust``

.. _`rocm_hipCUB`:

hipCUB
""""""

The `hipCUB <https://rocmdocs.amd.com/en/latest/ROCm_Libraries/ROCm_Libraries.html#hipcub>`_ library.
hipCUB is a thin wrapper library on top of rocPRIM or CUB.

Targets Created:

- ``ROCm::hipcub``

Result variables
^^^^^^^^^^^^^^^^

``ROCmToolkit_FOUND``
    A boolean specifying whether or not the ROCm Toolkit was found.

``ROCmToolkit_VERSION``
    The exact version of the ROCm Toolkit found.

``ROCmToolkit_VERSION_MAJOR``
    The major version of the ROCm Toolkit.

``ROCmToolkit_VERSION_MAJOR``
    The minor version of the ROCm Toolkit.

``ROCmToolkit_VERSION_PATCH``
    The patch version of the ROCm Toolkit.

``ROCmToolkit_BIN_DIR``
    The path to the ROCm Toolkit library directory that contains the ROCm
    executable ``hipcc``.

``ROCmToolkit_INCLUDE_DIRS``
    The path to the ROCm Toolkit ``include`` folder containing the header files
    required to compile a project linking against ROCm.

#]=======================================================================]

# Search using just using ROCmToolkit_ROOT
find_program(ROCmToolkit_HIPCC_EXECUTABLE
  NAMES hipcc hipcc.exe
  PATH_SUFFIXES bin
  NO_CMAKE_PATH
  NO_CMAKE_ENVIRONMENT_PATH
  NO_SYSTEM_ENVIRONMENT_PATH
  NO_CMAKE_SYSTEM_PATH
)

# If the user specified ROCmToolkit_ROOT but hipcc could not be found, this is an error.
if (DEFINED ROCmToolkit_ROOT OR DEFINED ENV{ROCmToolkit_ROOT})
  # Declare error messages now, print later depending on find_package args.
  set(fail_base "Could not find hipcc executable in path specified by")
  set(rocm_root_fail "${fail_base} ROCmToolkit_ROOT=${ROCmToolkit_ROOT}")
  set(env_rocm_root_fail "${fail_base} environment variable ROCmToolkit_ROOT=$ENV{ROCmToolkit_ROOT}")

  if (ROCmToolkit_FIND_REQUIRED)
    if (DEFINED ROCmToolkit_ROOT)
      message(FATAL_ERROR ${rocm_root_fail})
    elseif (DEFINED ENV{ROCmToolkit_ROOT})
      message(FATAL_ERROR ${env_rocm_root_fail})
    endif()
  else()
    if (NOT ROCmToolkit_FIND_QUIETLY)
      if (DEFINED ROCmToolkit_ROOT)
        message(STATUS ${rocm_root_fail})
      elseif (DEFINED ENV{ROCmToolkit_ROOT})
        message(STATUS ${env_rocm_root_fail})
      endif()
    endif()
    set(ROCmToolkit_FOUND FALSE)
    unset(fail_base)
    unset(rocm_root_fail)
    unset(env_rocm_root_fail)
    return()
  endif()
endif()

# Search first the HINTS ROCM_PATH and HIP_PATH env variables
# if not will fall back to looking in PATH
if(NOT ROCmToolkit_HIPCC_EXECUTABLE)
  find_program(ROCmToolkit_HIPCC_EXECUTABLE
    NAMES hipcc hipcc.exe
    HINTS ENV ROCM_PATH
          ENV HIP_PATH
    PATH_SUFFIXES bin
    NO_PACKAGE_ROOT_PATH
  )
endif()


# ROCmToolkit_ROOT cmake / env variable not specified, try platform defaults.
#
# - Linux: /opt/rocm-X.Y.Z
#
# We will also search the default symlink location /opt/rocm first since
# if ROCmToolkit_ROOT is not specified, it is assumed that the symlinked
# directory is the desired location.
if (NOT ROCmToolkit_HIPCC_EXECUTABLE)
  if (UNIX)
    set(platform_base "/opt/rocm-")
  else()
    message(FATAL_ERROR "ROCMToolkit only offically support Linux")
  endif()

  # Build out a descending list of possible cuda installations, e.g.
  file(GLOB possible_paths "${platform_base}*")
  # Iterate the glob results and create a descending list.
  set(versions)
  foreach (p ${possible_paths})
    # Extract version number from end of string
    string(REGEX MATCH "[0-9][0-9]?\\.[0-9]\\.[0-9]$" p_version ${p})
    if (IS_DIRECTORY ${p} AND p_version)
      list(APPEND versions ${p_version})
    endif()
  endforeach()

  # Sort numerically in descending order, so we try the newest versions first.
  list(SORT versions COMPARE NATURAL ORDER DESCENDING)

  # With a descending list of versions, populate possible paths to search.
  set(search_paths)
  foreach (v ${versions})
    list(APPEND search_paths "${platform_base}${v}")
  endforeach()

  # Force the global default /opt/rocm to the front on Unix.
  if (UNIX)
    list(INSERT search_paths 0 "/opt/rocm")
  endif()

  # Now search for hipcc again using the platform default search paths.
  find_program(ROCmToolkit_HIPCC_EXECUTABLE
    NAMES hipcc hipcc.exe
    HINTS ${search_paths}
    PATH_SUFFIXES bin
  )

  # We are done with these variables now, cleanup for caller.
  unset(platform_base)
  unset(possible_paths)
  unset(possible_versions)
  unset(versions)
  unset(i)
  unset(early_terminate)
  unset(search_paths)

  if (NOT ROCmToolkit_HIPCC_EXECUTABLE)
    if (ROCmToolkit_FIND_REQUIRED)
      message(FATAL_ERROR "Could not find hipcc, please set ROCmToolkit_ROOT.")
    elseif(NOT ROCmToolkit_FIND_QUIETLY)
      message(STATUS "Could not find hipcc, please set ROCmToolkit_ROOT.")
    endif()

    set(ROCmToolkit_FOUND FALSE)
    return()
  endif()
endif()

if(NOT ROCmToolkit_BIN_DIR AND ROCmToolkit_HIPCC_EXECUTABLE)
  get_filename_component(rocm_dir "${ROCmToolkit_HIPCC_EXECUTABLE}" DIRECTORY)
  set(ROCmToolkit_BIN_DIR "${rocm_dir}" CACHE PATH "" FORCE)
  mark_as_advanced(ROCmToolkit_BIN_DIR)
  unset(rocm_dir)
endif()

if(ROCmToolkit_HIPCC_EXECUTABLE)
  # Compute the version by invoking hipcc
  execute_process (COMMAND ${ROCmToolkit_HIPCC_EXECUTABLE} "--version" OUTPUT_VARIABLE hipcc_OUT)
  if(hipcc_OUT MATCHES [=[ V([0-9]+)\.([0-9]+)\.([0-9]+)]=])
    set(ROCmToolkit_VERSION_MAJOR "${CMAKE_MATCH_1}")
    set(ROCmToolkit_VERSION_MINOR "${CMAKE_MATCH_2}")
    set(ROCmToolkit_VERSION_PATCH "${CMAKE_MATCH_3}")
    set(ROCmToolkit_VERSION  "${CMAKE_MATCH_1}.${CMAKE_MATCH_2}.${CMAKE_MATCH_3}")
  endif()
  unset(hipcc_OUT)
endif()


get_filename_component(ROCmToolkit_ROOT_DIR ${ROCmToolkit_BIN_DIR} DIRECTORY ABSOLUTE)


# Find the include directory, and the hip host/device libraries
list(PREPEND CMAKE_FIND_ROOT_PATH "${ROCmToolkit_ROOT_DIR}")
find_package(hip CONFIG)

if(NOT hip_FOUND)
  message(FATAL_ERROR "ROCMToolkit unable to find the assoicated hip config find_package")
endif()

set(ROCmToolkit_INCLUDE_DIR "${hip_INCLUDE_DIR}")

#-----------------------------------------------------------------------------
# Perform version comparison and validate all required variables are set.
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(ROCmToolkit
  REQUIRED_VARS
    ROCmToolkit_INCLUDE_DIR
    ROCmToolkit_HIPCC_EXECUTABLE
  VERSION_VAR
    ROCmToolkit_VERSION
)
mark_as_advanced(ROCmToolkit_INCLUDE_DIR
                 ROCmToolkit_HIPCC_EXECUTABLE
                 )

#-----------------------------------------------------------------------------
# Construct result variables
if(ROCmToolkit_FOUND)
 set(ROCmToolkit_INCLUDE_DIRS ${ROCmToolkit_INCLUDE_DIR})
endif()

#-----------------------------------------------------------------------------
# Construct import targets
if(ROCmToolkit_FOUND)

  if(NOT TARGET ROCm::hip_host)
    add_library(ROCm::hip_host IMPORTED INTERFACE)
    target_link_libraries(ROCm::hip_host INTERFACE hip::host)
  endif()

  if(NOT TARGET ROCm::hip_device)
    add_library(ROCm::hip_device IMPORTED INTERFACE)
    target_link_libraries(ROCm::hip_device INTERFACE hip::device)
  endif()

  function(_ROCMToolkit_find_and_add_import_lib lib_name)
    if(TARGET ROCm::${lib_name})
      return()
    endif()

    cmake_parse_arguments(arg "" "" "IMPORTED_NAMESPACE;REQUIRES;EXTRA_LIBS" ${ARGN})
    set(_deps_satisfied True)
    foreach(dep IN LISTS arg_REQUIRES)
      if(NOT TARGET ROCm::${dep})
          set(_deps_satisfied False)
      endif()
    endforeach()

    set(imported_namespace roc::)
    if(arg_IMPORTED_NAMESPACE)
      set(imported_namespace "${arg_IMPORTED_NAMESPACE}::")
    endif()
    if(_deps_satisfied)

      # We should only look for the ROCm components that
      # are related to this installation of the toolkit
      find_package(${lib_name} CONFIG QUIET
          PATHS "${ROCmToolkit_ROOT_DIR}"
          NO_DEFAULT_PAT
      )
      set(imported_target "${imported_namespace}${lib_name}")
      if(TARGET ${imported_target})
        add_library(ROCm::${lib_name} IMPORTED INTERFACE)
        target_link_libraries(ROCm::${lib_name} INTERFACE ${imported_target})

        foreach(extra_lib IN LISTS arg_EXTRA_LIBS)
          set(imported_target "${imported_namespace}${extra_lib}")
          if(TARGET ${imported_target})
            add_library(ROCm::${extra_lib} IMPORTED INTERFACE)
            target_link_libraries(ROCm::${extra_lib} INTERFACE ${imported_target})
          endif()
        endforeach()
      endif()

    endif()
  endfunction()

  _ROCMToolkit_find_and_add_import_lib(rocblas)
  _ROCMToolkit_find_and_add_import_lib(hipblas)

  _ROCMToolkit_find_and_add_import_lib(rocrand)
  _ROCMToolkit_find_and_add_import_lib(rocfft EXTRA_LIBS rocfft-device)

  _ROCMToolkit_find_and_add_import_lib(rocprim)
  _ROCMToolkit_find_and_add_import_lib(rocsparse)
  _ROCMToolkit_find_and_add_import_lib(hipsparse)

  _ROCMToolkit_find_and_add_import_lib(rocsolver REQUIRES rocblas)

  _ROCMToolkit_find_and_add_import_lib(rocalution REQUIRES rocprim rocsparse rocblas)

  _ROCMToolkit_find_and_add_import_lib(rocthrust REQUIRES rocprim)
  _ROCMToolkit_find_and_add_import_lib(hipcub REQUIRES rocprim IMPORTED_NAMESPACE hip)

endif()

list(REMOVE_AT CMAKE_FIND_ROOT_PATH 0)
