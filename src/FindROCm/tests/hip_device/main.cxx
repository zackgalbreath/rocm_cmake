
#include <math.h>
#include <stdlib.h>

#include "body.h"
#include "hip/hip_runtime.h"

void LauncHipKernel(float, Body*, int);

void randomizeBodies(float *data, int n) {
  for (int i = 0; i < n; i++) {
    data[i] = 2.0f * (rand() / (float)RAND_MAX) - 1.0f;
  }
}

int main(const int argc, const char** argv) {

  int nBodies = 30000;
  if (argc > 1) nBodies = atoi(argv[1]);

  const float dt = 0.01f; // time step
  const int nIters = 10;  // simulation iterations

  int bytes = nBodies*sizeof(Body);
  float *buf = (float*)malloc(bytes);
  Body *p = (Body*)buf;

  randomizeBodies(buf, 6*nBodies); // Init pos / vel data

  float *d_buf;
  hipMalloc(&d_buf, bytes);
  Body *d_p = (Body*)d_buf;

  for (int iter = 1; iter <= nIters; iter++) {
    hipMemcpy(d_buf, buf, bytes, hipMemcpyHostToDevice);
    LauncHipKernel(dt, d_p, nBodies);
    hipMemcpy(buf, d_buf, bytes, hipMemcpyDeviceToHost);

    for (int i = 0 ; i < nBodies; i++) { // integrate position
      p[i].x += p[i].vx*dt;
      p[i].y += p[i].vy*dt;
      p[i].z += p[i].vz*dt;
    }
  }

  free(buf);
  hipFree(d_buf);
}
