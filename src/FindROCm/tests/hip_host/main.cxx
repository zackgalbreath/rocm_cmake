
#include <hip/hip_runtime_api.h>

int main()
{
  float* device_ptr;
  hipMalloc(&device_ptr, 1024 * sizeof(float));

  hipFree(device_ptr);
  return 0;
}
