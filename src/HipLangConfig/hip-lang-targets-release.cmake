#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "hip-lang::amdhip64" for configuration "Release"
set_property(TARGET hip-lang::amdhip64 APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(hip-lang::amdhip64 PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "hsa-runtime64::hsa-runtime64"
  IMPORTED_LOCATION_RELEASE "/opt/rocm-4.0.1/hip/lib/libamdhip64.so.4.0.40001"
  IMPORTED_SONAME_RELEASE "libamdhip64.so.4"
  )

list(APPEND _IMPORT_CHECK_TARGETS hip-lang::amdhip64 )
list(APPEND _IMPORT_CHECK_FILES_FOR_hip-lang::amdhip64 "/opt/rocm-4.0.1/hip/lib/libamdhip64.so.4.0.40001" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
