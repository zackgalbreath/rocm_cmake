
#!/bin/bash

if [ "$1" == "-no-clean" ]; then
  true
else
  rm -rf build_clang
  rm -rf build_hipcc
  rm -rf build_guessed
fi

# HIPCXX=/opt/rocm/llvm/bin/clang++ CXX=/opt/rocm/llvm/bin/clang++ /workspaces/build/bin/cmake -S . -B build_clang -DCMAKE_HIP_COMPILER_TOOLKIT_ROOT=/usr/local
HIPCXX=/opt/rocm/llvm/bin/clang++ CXX=/opt/rocm/llvm/bin/clang++ /workspaces/build/bin/cmake -S . -B build_clang
HIPCXX=/opt/rocm/bin/hipcc CXX=/opt/rocm/bin/hipcc /workspaces/build/bin/cmake -S . -B build_hipcc
/workspaces/build/bin/cmake -S . -B build_guessed

/workspaces/build/bin/cmake --build build_clang
/workspaces/build/bin/cmake --build build_hip

